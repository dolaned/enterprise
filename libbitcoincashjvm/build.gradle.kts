import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val serializationVersion = "1.3.0-RC"
val exposedVersion = "0.34.2"

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}
repositories {
    gradlePluginPortal()
    mavenCentral()
    jcenter()
    google()
}


plugins {
    kotlin("jvm") // version "1.5.30"
    kotlin("kapt") // version "1.4.21"
    kotlin("plugin.serialization") version "1.5.31"
}

//val compileKotlin: KotlinCompile by tasks
//compileKotlin.kotlinOptions {
//    jvmTarget = "1.11"
//}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

dependencies {
    //implementation fileTree(dir: 'libs', include: ['*.jar'])
    //implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    // implementation project(path: ':libbitcoincash')

    implementation(kotlin("reflect"))
    implementation(kotlin("stdlib-jdk8")) // jdk8 works for 11

    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.5.2")
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-cbor", serializationVersion)
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", serializationVersion)
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-json", serializationVersion)

    implementation("org.jetbrains.exposed", "exposed-core", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-dao", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-jdbc", exposedVersion)

    // gradle database dependencies described here: https://github.com/JetBrains/Exposed/wiki/DataBase-and-DataSource
    implementation("org.xerial","sqlite-jdbc","3.36.0.3")
    //implementation("com.h2database","h2","1.4.200")

}

/*
sourceSets {
    main {
        java.srcDir("src/core/java")
    }
}
 */


tasks {
    test {
        testLogging.showExceptions = true
    }
}