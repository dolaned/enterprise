import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

plugins {
    kotlin("jvm") // version "1.5.30"
    id("org.jetbrains.compose")
    id("java")
}

group = "me.stone"
version = "1.0.0"
val exposedVersion="0.34.1"

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
}

dependencies {
    testImplementation(kotlin("test-junit"))
    testImplementation("org.slf4j:slf4j-simple:1.7.32")
    // For test code that interfaces with a local bitcoind running json-rpc:
    testImplementation("wf.bitcoin:bitcoin-rpc-client:1.1.0")

    implementation(compose.desktop.currentOs)

    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
    implementation("org.slf4j:slf4j-simple:1.7.32")
    // JSON Serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.0.1")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.1")

    // Database
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    // gradle database dependencies described here: https://github.com/JetBrains/Exposed/wiki/DataBase-and-DataSource
    implementation("org.xerial:sqlite-jdbc:3.36.0.2")
    //implementation("com.h2database:h2:1.4.197")

    // This lets the embedded kotlin load native classes
    implementation("net.java.dev.jna:jna-platform:5.5.0")

    // Scripting
    implementation(kotlin("reflect"))
    implementation(kotlin("script-util"))
    implementation(kotlin("script-runtime"))
    implementation(kotlin("compiler-embeddable"))
    implementation(kotlin("scripting-compiler-embeddable"))
    implementation(kotlin("scripting-jsr223"))

    // LibBitcoinCash can be pulled in via release, jar file, or by symlinking the project into this one
    //implementation "info.bitcoinunlimited:libbitcoincash:0.2.1"
    //implementation(files(File("jars/libbitcoincashjvm.jar"))) // locate this directory at: <repo_home>/jars/
    implementation(project(":libbitcoincashjvm", "default"))

    implementation("com.google.zxing:core:3.4.0")
    // Maybe:  compile 'com.google.zxing:javase:3.4.0'
}

/*
tasks {
      java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(11))
        }
    }
}
*/

tasks.test {
    useJUnit()
}


tasks.withType<KotlinCompile>() {
    //kotlinOptions.jvmTarget = "11"
}

compose.desktop {
    application {
        mainClass = "bitcoinunlimited.enterprise.DesktopWallet"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "enterprise"
        }
    }
}
