// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.enterprise
import bitcoinunlimited.libbitcoincash.*
import kotlinx.coroutines.*
import org.junit.Test
import wf.bitcoin.javabitcoindrpcclient.BitcoinJSONRPCClient
import wf.bitcoin.javabitcoindrpcclient.BitcoinRPCException
import java.lang.IllegalStateException
import java.math.BigDecimal
import java.security.MessageDigest
import java.text.DecimalFormat
import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter
import kotlin.random.Random

val BCHregtestRpcPort = 18332  // Regtest RPC is the same as testnet port

val btcFormat = DecimalFormat("##,##0.########")

private val LogIt = Logger.getLogger("BU.wallet.TEST")

fun mBCHtoSAT(mBCH: BigDecimal): Long
{
    return (mBCH*100.toBigDecimal()*1000.toBigDecimal()).toLong()
}
fun mBCHtoSAT(mBCH: Long): Long = mBCHtoSAT(mBCH.toBigDecimal())

fun BCHtoSAT(mBCH: BigDecimal): Long
{
    return (mBCH*100.toBigDecimal()*1000000.toBigDecimal()).toLong()
}
fun BCHtoSAT(mBCH: Long): Long = mBCHtoSAT(mBCH.toBigDecimal())


fun mycheck(b:Boolean, onFail: (()->String?)? = null)
{
    if  (!b)
    {
        val v = onFail?.invoke()
        if (v != null) LogIt.severe(v)
        runBlocking {  delay(1000000) }
        throw IllegalStateException(v)
    }
}

suspend fun waitUntil(timeout:Long, predicate: ()->Boolean):Boolean
{
    var countdown = timeout
    while(!predicate())
    {
        LogIt.finer("Waiting  " + timeout + " at " + countdown)
        if (countdown<=0)
        {
            return false
        }
        min(countdown, 500).let {
            delay(it)
            countdown-=it
        }
    }
    return true
}



class Test
{
    init
    {
        InitializeJvmDesktopVariant()
    }

    @Test
    fun autoTest()
    {
        LogIt.level = Level.INFO
        val consoleHandler = ConsoleHandler()
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1\$tc] %4\$s: %5\$s\n")
        val formatter = SimpleFormatter()
        consoleHandler.formatter = formatter
        LogIt.addHandler(ConsoleHandler())
        allTests("rpc", "rpcrpc", "localhost", BCHregtestRpcPort)
    }


fun allTests(rpcuser: String, rpcpassword: String, ip: String, port:Int)
{
    TestPersist()
    TestHash()
    TestKeyDerivation()

    runBlocking {
        TestRequestMgr(rpcuser, rpcpassword, ip, port)
    }

    val genesisHash = Hash256("aa258934f701130c37bba436aa497c2dcd25b884ef1f4f4ee80598fa76e81526")

    val rpcConnection = "http://" + rpcuser + ":" + rpcpassword + "@" + ip + ":" + port
    LogIt.info("Connecting to: " + rpcConnection)
    var rpc = BitcoinJSONRPCClient(rpcConnection)
    val peerInfo = rpc.peerInfo
    mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    var blockcount = rpc.blockCount.toLong()

    // Test repeated wallet sync
    LogIt.info("Repeated Wallet Sync Test")
    runBlocking {
        val cnxnMgr = RegTestCnxnMgr("BCtempR","127.0.0.1", BCHregtestPort)
        val bchChain = Blockchain(ChainSelector.BCHREGTEST, "BCtempR", cnxnMgr, genesisHash, Hash256(), genesisHash, 0, 2.toBigInteger(), PlatformContext(), "rbch")
        bchChain.deleteAllPersistentData()
        bchChain.setTestDelayIntervals()
        GlobalScope.launch { cnxnMgr.run() }
        bchChain.start()

        for (cnt in range(0,5))
        {
            LogIt.info("Sync ${cnt}")
            val w = RamWallet("BCtempR" + cnt, bchChain.chainSelector)
            w.addBlockchain(bchChain, 0, null)
            mycheck(
                waitUntil(20000, { w.synced(blockcount) }),
                { w.chainstate!!.chain.name + ": Blockchain did not sync: " + w.chainstate!!.chain.curHeight + "  Expecting: " + blockcount })
            w.stop()
        }
        bchChain.stop()
        cnxnMgr.stop()
    }


    // Basic wallet functional test
    LogIt.info("Basic wallet functional test")
    runBlocking {
        val cnxnMgr = RegTestCnxnMgr("BCtemp","127.0.0.1", BCHregtestPort)
        val bchChain = Blockchain(ChainSelector.BCHREGTEST, "BCtemp", cnxnMgr, genesisHash, Hash256(), genesisHash, 0, 2.toBigInteger(), PlatformContext(), "rbch")
        bchChain.deleteAllPersistentData()
        bchChain.setTestDelayIntervals()
        GlobalScope.launch { cnxnMgr.run() }
        bchChain.start()
        delay(2000)

        LogIt.info("Test wallet syncing from genesis")
        val w = RamWallet("BCtemp1", bchChain.chainSelector)
        w.addBlockchain(bchChain, bchChain.checkpointHeight, null)  // Test syncing from tip
        TestWallet(w, rpcuser, rpcpassword, ip, port)

        LogIt.info("Test wallet from tip")
        val w2 = RamWallet("BCtemp2", bchChain.chainSelector)
        w2.addBlockchain(bchChain, 0, null)  // Test starting from 0
        TestWallet(w2, rpcuser, rpcpassword, ip, port)

        LogIt.info("TestWallet tests complete, stopping temporary blockchain")
        bchChain.stop()
        cnxnMgr.stop()
        w.stop()
        w2.stop()
    }

    LogIt.info("Wallet Rewind Test")
    runBlocking {
        val cnxnMgr = RegTestCnxnMgr("BCtemp1","127.0.0.1", BCHregtestPort)
        val bchChain = Blockchain(ChainSelector.BCHREGTEST, "BCtemp1", cnxnMgr, genesisHash, genesisHash, genesisHash, 0, 2.toBigInteger(), PlatformContext(), "rbch2")
        bchChain.deleteAllPersistentData()
        bchChain.setTestDelayIntervals()
        GlobalScope.launch { cnxnMgr.run() }
        bchChain.start()
        delay(2000)

        LogIt.info("Test wallet syncing from genesis")
        val w = RamWallet("BCtemp2", bchChain.chainSelector)
        w.addBlockchain(bchChain, bchChain.checkpointHeight, null)
        delay(5000)  // Wait for wallet to install bloom, etc

        TestWalletRewind(w, rpc)

        LogIt.info("TestWallet tests complete, stopping temporary blockchain")
        bchChain.stop()
        cnxnMgr.stop()
        w.stop()
    }

    LogIt.info("ALL TESTS COMPLETE")
}

fun TestPersist()
{
    LogIt.info("TestPersist")

    val db = DesktopDb("test")
    val dao = db.blockHeaderDao()
    var bh = BlockHeader()
    dao.delete(bh)
    PersistInsert(dao, bh)
    bh.hashData.hash[0] = 1
    dao.delete(bh)
    PersistInsert(dao, bh)

}

fun TestHash()
{
    LogIt.info("TestHash")

    // TODO try a lot of random data
    val tmp = "010000000393b0f317ac53fcd6781d0f6c38a1d182290930f0dd513be38d769e3b146d5e62000000006a473044022043643689bec00bd1321df7f971945cf54f7e447377e9d9b74ebb56d84af729f4022035445b973377bbf186e4df4361de1d79d98ce2e5af051f329f44e5b377236e3f412102e630f56e18b49653a2f679d99140e921986c7ff0939daf3eaefac57b98535d0dfeffffffa9886a7905dd0f71f3056758ce725a155ae427a80335ccbde40a5dde92cb73c10100000069463043021f37a063ef506db5f808ad40a0132ac888ae0407abc3c15cf5cf7535a3a0b0990220069cc4dce75a217d757e238740dd0cdc9bf733ba414e12ed54a3f901dddf7d8041210228aeddf6c7a1c943c1d379a3a5218c5676bbc95f89f005481efa5a79f69be960fefffffff0c1567649ea846ea4b16201f62ee1c74622bb7f3f16acf8ce02a3ec7814d63d000000006b483045022100add55f73990267a6516cb32b30e65b045025c04643a6468c0758a86853562a590220676c995386121590e7c56f7d3570c2e3d029fba004f63a72fb652bcc9876b53d412103c37c0afa84d74f836a62aa9c7549f58979371aa09179e36e66ce4280f0d08b6bfeffffff02b4852000000000001976a914523c4b26ea97d29deebc8477017f478519b7bf5288acc0ce7217000000001976a9145e8ee27109bed3a46c688d123bfa9c21da69426388acec000000"
    val bin = tmp.fromHex()
    val jnihash = Hash.hash256(bin)
    val step1 = MessageDigest.getInstance("SHA-256").digest(bin)
    val hash = MessageDigest.getInstance("SHA-256").digest(step1)
    if (!(hash contentEquals jnihash))
    {
        throw IllegalStateException("Hash algorithm issue")
    }
}

fun TestKeyDerivation()
{
    LogIt.info("TestKeyDerivation")
    val secret = SecRandom(ByteArray(32))

    for (i in 0..1000)
    {
        val result = AddressDerivationKey.Hd44DeriveChildKey(secret, 44, AddressDerivationKey.BTC, 0, 1, i)
        mycheck(result.size == 32, { "derived key too small: " + result.toHex() })
        LogIt.finest(result.toHex())
    }
}

suspend fun TestRequestMgr(rpcuser: String, rpcpassword: String, ip: String, port:Int)
{
    LogIt.info("TestRequestMgr")
    val rpcConnection = "http://" + rpcuser + ":" + rpcpassword + "@" + ip + ":" + port
    LogIt.info("Connecting to: " + rpcConnection)
    var rpc = BitcoinJSONRPCClient(rpcConnection)

    val cnxnMgr = RegTestCnxnMgr("BC1","127.0.0.1", BCHregtestPort)
    val reqMgr = RequestMgr(cnxnMgr, Hash256())  // TODO fill in genesis block
    GlobalScope.launch { cnxnMgr.run() }
    Thread.sleep(1000) // Let the connection come up
    if (cnxnMgr.getAnotherNode(setOf()) == null)
    {
        println("\n*** To run this test, you must run a nextchain regtest full node on localhost! ***")
        throw P2PNoNodesException()
    }

    // Check that the headers read are the same as the blocks loaded via RPC
    val hdrs = reqMgr.getBlockHeaders(Hash256())
    var height = 1
    for (hdr in hdrs)
    {
        val rpcBlock = rpc.getBlock(height)
        //println(rpcBlock.hash())
        //println(hdr.hash.toHex())
        mycheck(rpcBlock.hash() == hdr.hash.toHex())
        height += 1
    }

    // Test the getBlockHeaderByHash function
    height = 1
    for (hdr in hdrs)
    {
        val rpcBlock = rpc.getBlock(height)

        val hdr2 = reqMgr.getBlockHeaderByHash(hdr.hash)!!
        //println(rpcBlock.hash())
        //println(hdr.hash.toHex())
        //println(hdr2.hash.toHex())
        mycheck(rpcBlock.hash() == hdr2.hash.toHex())
        height += 1
    }

    // Test the getBlock function
    for (hdr in hdrs)
    {
        val txes = reqMgr.requestTxInBlock(Guid(hdr.hash))
        val rpcBlock = rpc.getBlock(Guid(hdr.hash).toHex())
        mycheck(txes.size == rpcBlock.tx().size)
        val lkup = mutableMapOf<String, String>()
        for(t in rpcBlock.tx())
        {
            lkup[t] = t
        }
        for(i in 0..txes.size-1)
        {
            mycheck(txes[i].hash.toHex() in lkup)
        }

    }
}


suspend fun TestWallet(wallet: Wallet, rpcuser: String, rpcpassword: String, ip: String, port:Int)
{
    LogIt.info("TestWallet")
    mycheck(wallet.balance == 0.toLong(), { wallet.name + ": Test Malfunction, initial balance incorrect" })

    val dbgWal = wallet as RamWallet
    val rpcConnection = "http://" + rpcuser + ":" + rpcpassword + "@" + ip + ":" + port
    LogIt.info("Connecting to: " + rpcConnection)
    var rpc = BitcoinJSONRPCClient(rpcConnection)
    val peerInfo = rpc.peerInfo
    mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    var blockcount = rpc.blockCount.toLong()
    //println("Rpc " + blockcount)
    //println("Blockchain " + wallet.chainstate!!.chain.curHeight)
    //println("Wallet " + wallet.chainstate!!.syncedHeight)
    mycheck(waitUntil(10000, { wallet.chainstate!!.chain.curHeight == blockcount}), { wallet.chainstate!!.chain.name + ": Blockchain did not sync: " + wallet.chainstate!!.chain.curHeight + "  Expecting: " + blockcount})


    // Test sending to this wallet
    val myAddr = wallet.newDestination()
    //LogIt.info(wallet.coinTicker +": wallet addresses quantity: " + dbgWal.receiving.size)
    var txrpc = rpc.sendToAddress(myAddr.address.toString(), 1.toBigDecimal())
    LogIt.info("Wallet initial height: " + dbgWal.syncedHeight)
    LogIt.info("Seed addr: " + myAddr.address.toString() + " Seed tx:" + txrpc.toString())

    blockcount = rpc.blockCount.toLong()
    //println("Rpc " + blockcount)
    //println("Blockchain " + wallet.chainstate!!.chain.curHeight)
    //println("Wallet " + wallet.chainstate!!.syncedHeight)
    mycheck(waitUntil(15000, { wallet.synced(blockcount)}), { wallet.name + ": Wallet did not initially sync.  At " + dbgWal.syncedHeight + ". Chain is at height: " + rpc.blockCount})

    mycheck(waitUntil(10000, { wallet.balanceUnconfirmed == mBCHtoSAT(1000)}), { wallet.name + ": Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(100)})
    mycheck(wallet.balance == 0.toLong(), { wallet.name + ": Confirmed balance incorrect" })

    rpc.generate(1)
    var height = rpc.getBlockCount().toLong()
    mycheck(waitUntil(10000, { wallet.synced(height)}), { wallet.name + ": Wallet did not sync to " + height + ".  Its at height: " + rpc.blockCount})

    mycheck(wallet.balanceUnconfirmed == 0.toLong(), { wallet.name + ": Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString()})
    mycheck(wallet.balance == mBCHtoSAT(1000), { wallet.name + ": Confirmed balance incorrect, is: " + wallet.balance.toString()})


    // Test sending back to bitcoind
    val startingBal = wallet.balance
    val otherAddr = rpc.getNewAddress()
    var tx:BCHtransaction = wallet.send(10000, otherAddr)
    LogIt.info("Send back to bitcoind: " + tx.hash)

    mycheck(waitUntil(2000, { wallet.balance == 0.toLong()}))  // we used up all the utxos in this wallet so the full balance is not unconfirmed
    mycheck(wallet.balanceUnconfirmed  <= startingBal - 10000)  // it'll be less than because of tx fees
    var unconfBal = wallet.balanceUnconfirmed

    // check that the transaction we sent was accepted in the mempool
    mycheck(waitUntil(10000, { rpc.rawMemPool.size >= 1 }), { "Transaction was not accepted by bitcoind" } )
    rpc.generate(1)
    // Check that the transaction we sent was mined
    var mempool = rpc.getRawMemPool()
    mycheck(mempool.size == 0)

    height = rpc.blockCount.toLong()
    mycheck(waitUntil(10000, {wallet.synced(height)}), { "wallet did not sync to " + height + ".  Its at height: " + rpc.blockCount})

    // Check that the balance became confirmed once the wallet synced with the new block
    mycheck((wallet.balance < startingBal)&&(wallet.balance > 0), { "Balance: " + wallet.balance + "  Expected: " + unconfBal })
    mycheck(wallet.balanceUnconfirmed == 0.toLong())

    var nextDestination = wallet.newDestination()
    delay(1000)

    val dests1 = mutableListOf<PayDestination>()
    for (i in 1..5)
    {
        // Test sending to this wallet

        // Note, if we create a destination and then immediately instruct a node to send to it, there is a race condition between our registration of the new bloom filters into nodes and the send.  So instead we create the new destination
        // for the NEXT iteration of the loop at the beginning of this iteration.
        val myAddr1 = nextDestination
        nextDestination = wallet.newDestination()

        val qty1 = Random.nextInt(100, 10000).toBigDecimal(currencyMath).setScale(currencyScale)/1000.toBigDecimal(currencyMath)

        val bal1 = wallet.balance

        LogIt.info("Send " + qty1.toString() + " to " + myAddr1.address.toString())
        try
        {
            val result = rpc.sendToAddress(myAddr1.address.toString(), qty1)
            LogIt.info(result)
        }
        catch(e: Exception)
        {
            LogIt.info(e.message)
        }

        mycheck(waitUntil(4000, { wallet.balanceUnconfirmed == BCHtoSAT(qty1) }), { "Transaction was not noticed by wallet 1. Balance is " + wallet.balanceUnconfirmed + ".  Expecting " + BCHtoSAT(qty1) })

        mycheck(waitUntil(5000, { rpc.rawMemPool.size == 1 }))
        // Check that balance didn't change
        mycheck(wallet.balance == bal1)

        rpc.generate(1)

        mycheck(waitUntil(4000, { wallet.balance == bal1 + BCHtoSAT(qty1) }))

        dests1.add(myAddr1)

        val numTxes = Random.nextInt(20)+5
        LogIt.info("Wallet Balance: " + wallet.balance + " Unconfirmed: " + wallet.balanceUnconfirmed)
        val total = wallet.balance + wallet.balanceUnconfirmed
        for (j in 1..numTxes)
        {

            val a1 = dests1[Random.nextInt(0, dests1.size)]

            val qtytmp = Random.nextInt(10, 1000).toBigDecimal(currencyMath).setScale(currencyScale) / 1000.toBigDecimal(currencyMath)

            val txtmp = wallet.send(mBCHtoSAT(qtytmp), a1.outputScript())
            LogIt.info("Sent " + qtytmp + " mBCH to " + a1.address.toString() + " in TX: " + txtmp.hash.toHex() + ".  Balance: " + wallet.balance + "  Unconfirmed: " + wallet.balanceUnconfirmed)

            if (wallet.balance + wallet.balanceUnconfirmed < total - (j * 1000))  // I'm sending to myself so should only lose fees which we can estimate to be less than 1k SAT per tx
            {
                LogIt.severe("Balance problem!")

            }
        }


        // Make sure that my tx have propagated
        mycheck(waitUntil(10000, { rpc.rawMemPool.size == numTxes }))

        rpc.generate(1)

        // Generated block should have confirmed all of the tx I created
        mycheck(waitUntil(10000, { wallet.balanceUnconfirmed == 0.toLong() }), { "Transaction was not noticed by wallet 1.  Unconfirmed balance is " + wallet.balanceUnconfirmed })

        LogIt.info("loop " + i.toString())
    }

    LogIt.info("TestWallet completed")
}


suspend fun TestWalletRewind(wallet: Wallet, rpc: BitcoinJSONRPCClient)
{
    val dbgWal = wallet as RamWallet

    rpc.generate(1) // If a prior test left the blockchain in a "tie" state, break the tie so that this test starts cleanly

    // Test sending to this wallet
    val myAddr = wallet.newDestination()
    var txrpc = rpc.sendToAddress(myAddr.address.toString(), 10.toBigDecimal())
    LogIt.info("Wallet initial height: " + dbgWal.syncedHeight)
    LogIt.info("Initial funding tx:" + txrpc.toString())

    var height = rpc.blockCount.toLong()
    mycheck(waitUntil(15000, { wallet.synced(height)}), { "Wallet did not initially sync (at " + dbgWal.syncedHeight + ". Chain is at height: " + rpc.blockCount})

    mycheck(waitUntil(5000, { wallet.balanceUnconfirmed == mBCHtoSAT(10000)}), { "Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(100)})
    mycheck(wallet.balance == 0.toLong(), { "Confirmed balance incorrect" })

    val block1Hash = rpc.generate(1)[0]
    LogIt.info("Moving forward to " + block1Hash)
    height = rpc.blockCount.toLong()
    mycheck(waitUntil(10000,{ wallet.synced(height)}), { "wallet did not sync to " + height + ".  Its at height: " + rpc.blockCount})

    mycheck(waitUntil(10000, {wallet.balanceUnconfirmed == 0.toLong()}), { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString()})
    mycheck(waitUntil(4000,{wallet.balance == mBCHtoSAT(10000)}), { "Confirmed balance incorrect, is: " + wallet.balance.toString()})

    rpc.invalidateBlock(block1Hash)
    LogIt.info("Invalidated " + block1Hash + " abandoning tx " + txrpc.toString())
    // race condition abandoning the transaction so loop until it works.  (It may not have been re-processed yet after the block invalidate)
    mycheck(waitUntil(5000, { rpc.rawMemPool.contains(txrpc.toString()) }), { "unwound tx didn't appear in mempool" })
    val abandonRet = rpc.query("abandontransaction", txrpc)  // This RPC is not provided by the library so fall back to the query interface
    if (abandonRet != null) LogIt.info(abandonRet.toString())
    mycheck(rpc.getRawMemPool().size == 0, { "Expecting empty mempool, found: " + rpc.getRawMemPool().toString() + "\n  tx that should be abandoned: " + rpc.getTransaction(txrpc).toString() })
    val forkHashes = rpc.generate(2)
    LogIt.info("Forked with " + forkHashes[0] + " and " + forkHashes[1])

    // Wait for this wallet to sync against the new header chain
    mycheck(waitUntil(50000, { wallet.balanceUnconfirmed == 0.toLong()}), { "Post reorg, unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(0)})
    mycheck(waitUntil(1000000, { wallet.balance == 0.toLong() }), { "Post reorg confirmed balance incorrect, is " + wallet.balance })

    LogIt.info("TestWalletRewind simple rewind completed")
    // ----------------------------
    LogIt.info("Mempool size is " + rpc.rawMemPool.size)

    val myAddr2 = wallet.newDestination()
    txrpc = rpc.sendToAddress(myAddr.address.toString(), 1.toBigDecimal())
    rpc.sendToAddress(myAddr2.address.toString(), 2.toBigDecimal())

    mycheck(rpc.rawMemPool.size == 2)
    mycheck(wallet.synced(rpc.blockCount.toLong()), { "Wallet did not initially sync (at " + dbgWal.syncedHeight + ". Chain is at height: " + rpc.blockCount})

    mycheck(waitUntil(5000, { wallet.balanceUnconfirmed == mBCHtoSAT(3000)}), { "Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(3000)})
    mycheck(wallet.balance == 0.toLong(), { "Confirmed balance incorrect" })

    val block2Hash = rpc.generate(1)[0]

    val btcdAddr = rpc.getNewAddress()

    val tx2 = wallet.send(mBCHtoSAT(100), btcdAddr)
    LogIt.info("Send back to bitcoind: " + tx2.hash.toHex())

    mycheck(waitUntil(5000, { rpc.rawMemPool.size == 1 }), { "tx did not enter bitcoind mempool" })

    val block3Hash = rpc.generate(1)[0]

    mycheck(waitUntil(20000, { wallet.balanceUnconfirmed == 0.toLong()}), { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString()})
    mycheck(waitUntil(20000, {wallet.balance > mBCHtoSAT(3000 - 11)}), { "Confirmed balance incorrect, is: " + wallet.balance.toString()})  // > -11 because tx fee

    rpc.invalidateBlock(block3Hash)
    LogIt.info("Invalidated " + block3Hash)
    mycheck(waitUntil(5000, { rpc.rawMemPool.size > 0 }), { "invalidated block's tx did not enter mempool" })
    try  // I have to wait for it to insert before I try to evict
    {
        LogIt.info("evicting: " + tx2.hash.toHex())
        rpc.query("evicttransaction", tx2.hash.toHex())  // Clear my tx out of the node's mempool
    }
    catch (e: BitcoinRPCException)
    {

    }
    mycheck(waitUntil(5000, { rpc.rawMemPool.size == 0 }), { "can't remove tx from mempool" })

    rpc.generate(3)
    var btcdHeight = rpc.getBlockCount().toLong()

    wallet.synced(btcdHeight)
    // Now my send should be gone
    mycheck(wallet.balanceUnconfirmed == 0.toLong(), { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString()})
    mycheck(wallet.balance == mBCHtoSAT(3000), { "Confirmed balance incorrect, is: " + wallet.balance.toString()})

    rpc.invalidateBlock(block2Hash)
    mycheck(waitUntil(5000, { rpc.rawMemPool.size == 2 }), { "unwound tx didn't appear in mempool" })
    rpc.query("abandontransaction", txrpc)  // Abandon one but let the other replay

    rpc.generate(5)  // I need enough blocks to exceed the other fork before this wallet will move over

    mycheck(waitUntil(20000, { wallet.balanceUnconfirmed == 0.toLong()}), { "Post reorg, unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(0)})
    mycheck(waitUntil(20000, { wallet.balance == mBCHtoSAT(2000) }), { "Post reorg confirmed balance incorrect, is " + wallet.balance })


    LogIt.info("TestWalletRewind completed")
}

suspend fun TestMultiBlockchain(wallet1: Wallet, rpcuser: String, rpcpassword: String, ip: String, port:Int,
                               wallet2: Wallet, rpcuser2: String, rpcpassword2: String, ip2: String, port2:Int
)
{
    //var rpc1 = BitcoinJSONRPCClient("http://bu:bu@127.0.0.1:18332/")
    var rpc1 = BitcoinJSONRPCClient("http://" + rpcuser + ":" + rpcpassword + "@" + ip + ":" + port)
    val peerInfo = rpc1.peerInfo
    mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    var rpc2 = BitcoinJSONRPCClient("http://" + rpcuser2 + ":" + rpcpassword2 + "@" + ip2 + ":" + port2)
    val peerInfo2 = rpc2.peerInfo
    mycheck(peerInfo2.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    mycheck(wallet1.synced(rpc1.blockCount.toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc1.blockCount})
    mycheck(wallet2.synced(rpc2.blockCount.toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc2.blockCount})


    // Test sending to this wallet
    val myAddr1 = wallet1.newDestination()
    val myAddr2 = wallet2.newDestination()
    rpc1.sendToAddress(myAddr1.address.toString(), 0.1.toBigDecimal())
    rpc2.sendToAddress(myAddr2.address.toString(), 0.09.toBigDecimal())

    mycheck(waitUntil(10000, { wallet1.balanceUnconfirmed == BCHtoSAT(0.1.toBigDecimal())}), { "Transaction was not noticed by wallet 1" } )
    mycheck(waitUntil(10000, { wallet2.balanceUnconfirmed == BCHtoSAT(0.09.toBigDecimal())}), { "Transaction was not noticed by wallet 2" } )

    mycheck(wallet1.balance == 0.toLong())
    mycheck(wallet2.balance == 0.toLong())

    rpc2.generate(1)

    mycheck(waitUntil(10000, { wallet2.balance == BCHtoSAT(0.09.toBigDecimal())}), { "Confirmation was not noticed by wallet 2"})
    mycheck(wallet1.balanceUnconfirmed == BCHtoSAT(0.1.toBigDecimal()))  // finding a block on 2 should not affect 1
    mycheck(wallet1.balance == 0.toLong())  // finding a block on 2 should not affect 1

    rpc1.generate(1)

    mycheck(waitUntil(10000, {wallet1.balance == BCHtoSAT(0.1.toBigDecimal())}))
    mycheck(wallet2.balance == BCHtoSAT(0.09.toBigDecimal()))

    LogIt.info("TestMultiBlockchain completed")
}


fun analyzebug(txes: List<BCHtransaction>, rawMempool: List<String>):String
{
    val mytx:MutableSet<String> = mutableSetOf()
    val mptx:MutableSet<String> = mutableSetOf()
    for (t in txes) mytx.add(t.hash.toHex())
    for (tx in rawMempool) mptx.add(tx)

    val diff = mytx - mptx
    var s: String = "Missing TX: "
    for (txhash in diff)
    {
        s = s + txhash + " "
    }
    s = s + "\nMempool size: " + rawMempool.size + " Expected: " + mytx.size
    return s
}

suspend fun TestMultiBlockchainLongevity(wallet1: Wallet, rpcuser: String, rpcpassword: String, ip: String, port:Int,
                                wallet2: Wallet, rpcuser2: String, rpcpassword2: String, ip2: String, port2:Int
)
{
    //var rpc1 = BitcoinJSONRPCClient("http://bu:bu@127.0.0.1:18332/")
    var rpc1 = BitcoinJSONRPCClient("http://" + rpcuser + ":" + rpcpassword + "@" + ip + ":" + port)
    val peerInfo = rpc1.peerInfo
    mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    var rpc2 = BitcoinJSONRPCClient("http://" + rpcuser2 + ":" + rpcpassword2 + "@" + ip2 + ":" + port2)
    val peerInfo2 = rpc2.peerInfo
    mycheck(peerInfo2.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    mycheck(wallet1.synced(rpc1.blockCount.toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc1.blockCount})
    mycheck(wallet2.synced(rpc2.blockCount.toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc2.blockCount})

    val dests1 = mutableListOf<PayDestination>()
    val dests2 = mutableListOf<PayDestination>()

    for (i in 1..100000)
    {
        // Test sending to this wallet
        val myAddr1 = wallet1.newDestination()
        val myAddr2 = wallet2.newDestination()

        val qty1 = Random.nextInt(100, 10000).toBigDecimal(currencyMath).setScale(currencyScale)/1000.toBigDecimal(currencyMath)
        val qty2 = Random.nextInt(100, 10000).toBigDecimal(currencyMath).setScale(currencyScale)/1000.toBigDecimal(currencyMath)

        val bal1 = wallet1.balance
        val bal2 = wallet2.balance

        LogIt.info("Send " + qty1.toString() + " to " + myAddr1.address.toString())
        LogIt.info("Send " + qty2.toString() + " to " + myAddr2.address.toString())
        try
        {
            val tx1 = rpc1.sendToAddress(myAddr1.address.toString(), qty1)
            LogIt.info("Send " + qty1.toString() + " to " + myAddr1.address.toString() + " in tx " + tx1)
            val tx2 = rpc2.sendToAddress(myAddr2.address.toString(), qty2)
            LogIt.info("Send " + qty2.toString() + " to " + myAddr2.address.toString() + " in tx " + tx2)
        }
        catch(e: Exception)
        {
            LogIt.info(e.message)
        }

        mycheck(waitUntil(4000, { wallet1.balanceUnconfirmed == BCHtoSAT(qty1) }), { "Transaction was not noticed by wallet 1" })
        mycheck(waitUntil(4000, { wallet2.balanceUnconfirmed == BCHtoSAT(qty2) }), { "Transaction was not noticed by wallet 2" })

        // Check that balance didn't change
        mycheck(wallet1.balance == bal1)
        mycheck(wallet2.balance == bal2)

        rpc2.generate(1)

        mycheck(waitUntil(4000, { wallet2.balance == bal2 + BCHtoSAT(qty2) }), { "Confirmation was not noticed by wallet 2" })

        rpc1.generate(1)

        mycheck(waitUntil(4000, { wallet1.balance == bal1 + BCHtoSAT(qty1) }))

        dests1.add(myAddr1)
        dests2.add(myAddr2)

        val numTxes = Random.nextInt(24)

        var txes1:MutableList<BCHtransaction> = mutableListOf()
        var txes2:MutableList<BCHtransaction> = mutableListOf()
        for (j in 1..numTxes)
        {
            val a1 = dests1[Random.nextInt(0, dests1.size)]
            val a2 = dests2[Random.nextInt(0, dests2.size)]

            val qtytmp1 = Random.nextInt(10, 1000).toBigDecimal(currencyMath).setScale(currencyScale)/10000.toBigDecimal(currencyMath)
            val qtytmp2 = Random.nextInt(10, 1000).toBigDecimal(currencyMath).setScale(currencyScale)/10000.toBigDecimal(currencyMath)

            // These wallets are on different blockchains so I have to send to my own wallet here
            txes1.add(wallet1.send(BCHtoSAT(qtytmp1), a1.outputScript()))
            txes2.add(wallet2.send(BCHtoSAT(qtytmp2), a2.outputScript()))
        }

        // Make sure that my tx have propagated
        mycheck(waitUntil(10000, { rpc1.getRawMemPool().size == numTxes }),
            {
                val rawMempool = rpc1.getRawMemPool()
                analyzebug(txes1, rawMempool)
            })
        mycheck(waitUntil(20000, {
            val mpsize = rpc2.getRawMemPool().size
            LogIt.info("rpc2 mempool size " + mpsize)
            mpsize == numTxes
        }),
            {
                val rawMempool = rpc2.getRawMemPool()
                analyzebug(txes2, rawMempool)
                //"Mempool size: " + rpc2.rawMemPool.size + " Expected: " + numTxes
            })

        rpc2.generate(1)
        rpc1.generate(1)
        mycheck(waitUntil(4000, { wallet1.balanceUnconfirmed == 0.toLong() }), { "Transaction was not noticed by wallet 1" })
        mycheck(waitUntil(4000, { wallet2.balanceUnconfirmed == 0.toLong() }), { "Transaction was not noticed by wallet 2" })

        LogIt.info("loop " + i.toString())
    }

    LogIt.info("TestMultiBlockchainLongevity completed")
}

}