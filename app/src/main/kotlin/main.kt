// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.enterprise;

import bitcoinunlimited.libbitcoincash.*

import kotlinx.serialization.json.Json
import org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngine

// is eng: kotlin.script.experimental.jvmhost.jsr223.KotlinJsr223ScriptEngineImpl?
// YOU forgot create the file src/main/resources/META-INF/services/javax.script.ScriptEngineFactory with contents "org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngineFactory"

import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintStream
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.Executors
import javax.script.ScriptEngineManager
import javax.script.ScriptException
import kotlin.coroutines.CoroutineContext

import kotlinx.coroutines.*
import javax.script.ScriptEngine

import kotlinx.serialization.*
import kotlinx.serialization.json.*
import kotlin.NoSuchElementException
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path


private val LogIt = GetLog("BU.enterpriseWallet")

var scriptSearchPath:List<String> = mutableListOf<String>()

val runTests = false

val mBchDecimals = 5  //? The number of decimal places needed to express 1 Satoshi (or equivalent) in the units used in the GUI
val currencyScale = 16  //?  How many decimal places we need to do math without creating cumulative rounding errors
val fiatFormat = DecimalFormat("##,##0.00")  //? How all fiat currencies are displayed (2 decimal places)
val mBchFormat = DecimalFormat("##,##0.#####")  //? How the mBCH crypto is displayed (5 optional decimal places)
val currencyMath = MathContext(16, RoundingMode.HALF_UP)  //? tell the system details about how we want bigdecimal math handled

// val DEFAULT_CHAIN = ChainSelector.BCHREGTEST.v
val DEFAULT_CHAIN = ChainSelector.NEXTCHAIN.v

lateinit var XNEX: Blockchain
lateinit var BCH: Blockchain

val REGTEST2_IP = "127.0.0.1"

fun RegtestIP() = "192.168.1.100"

val hardCodedElectrumClients = mutableListOf<IpPort>(IpPort("electrumserver.seed.bitcoinunlimited.net", DEFAULT_TCP_ELECTRUM_PORT), IpPort("bitcoincash.network", DEFAULT_TCP_ELECTRUM_PORT))

val getElectrumServerCandidate: (ChainSelector) -> IpPort = { chain: ChainSelector ->
    println("getElectrumServerCandidate for ${chain}")
    when(chain)
    {
        ChainSelector.BCHMAINNET -> hardCodedElectrumClients.random()
        ChainSelector.BCHTESTNET -> IpPort("159.65.163.15", DEFAULT_TCP_ELECTRUM_PORT)
        ChainSelector.BCHREGTEST -> IpPort("127.0.0.1", DEFAULT_TCP_ELECTRUM_PORT)
        ChainSelector.NEXTCHAIN -> IpPort("192.168.1.100", 7229)
        ChainSelector.BCHNOLNET -> throw NotImplementedError()
    }
}

val hardCodedXecElectrumClients = mutableListOf<IpPort>(IpPort("electrum.bitcoinabc.org", DEFAULT_TCP_ELECTRUM_PORT), IpPort("fulcrum-main.bchjs.cash", DEFAULT_TCP_ELECTRUM_PORT),IpPort("bchabc.fmarcosh.xyz", DEFAULT_TCP_ELECTRUM_PORT))

val getXecElectrumServerCandidate: (ChainSelector) -> IpPort =  { chain: ChainSelector -> hardCodedXecElectrumClients.random() }


open class Error(what: String): BUException(what,"")

object DesktopWallet
{
    //var engine:KotlinJsr223JvmLocalScriptEngine? = null
    var engine:ScriptEngine? = null
    var wallet:MutableMap<String, Wallet> = mutableMapOf()
    var blockchains: MutableMap<String, Blockchain> = mutableMapOf()

    val inputLines = mutableListOf<String>()

    var lastFile:String = ""

    val coCtxt: CoroutineContext = Executors.newFixedThreadPool(4).asCoroutineDispatcher()
    val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

    fun initializeWallets(restart: Boolean = false)
    {
        val context = bitcoinunlimited.libbitcoincash.PlatformContext()
/*
        run {
            val name = "mBR1"
            val cnxnMgr = RegTestCnxnMgr(name,"127.0.0.1", BCHregtestPort)
            val bchChain = Blockchain(ChainSelector.BCHREGTEST, name, cnxnMgr, Hash256(), Hash256(), Hash256(), -1, -1.toBigInteger(), PlatformContext())
            if (restart) bchChain.deleteAllPersistentData()
            GlobalScope.launch { cnxnMgr.run() }
            bchChain.start()
            newWallet(name, bchChain)
        }

        run {
            val name = "mBR2"
            val cnxnMgr2 = RegTestCnxnMgr("mBR2", REGTEST2_IP, BCHregtest2Port)
            val bchChain2 = Blockchain(ChainSelector.BCHREGTEST, "mBR2", cnxnMgr2, Hash256(), Hash256(), Hash256(), -1, -1.toBigInteger(), PlatformContext())
            if (restart) bchChain2.deleteAllPersistentData()
            GlobalScope.launch { cnxnMgr2.run() }
            bchChain2.start()
            newWallet("mBR2", bchChain2)
        }
*/
        /*
        val context = PlatformContext()
        run {
            val name = "mTBCH"
            val cnxnMgr = MultiNodeCnxnMgr(name,ChainSelector.BCHTESTNET, "testnet-seed.bitcoinabc.org")
            val bchChain = Blockchain(ChainSelector.BCHTESTNET, name, cnxnMgr, Hash256("000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943"), Hash256("00000000000486c1ffaeae65361ee9ad67435f0f85eb0d4d80aaa93614760ab1"), Hash256("000000000004743cb213b6d956eecc1a301401cc305a20d9e723d37a69f9f247"), 1331680, "52601c9fa624e422a6".toBigInteger(16), context)
            if (restart) bchChain.deleteAllPersistentData()
            cnxnMgr.start()
            bchChain.start()
            newWallet(name, bchChain)
        }

        run {
            val name = "mRBCH"
            val cnxnMgr2 = RegTestCnxnMgr(name, RegtestIP(), mRBCHPort)
            val bchChain2 = Blockchain(ChainSelector.BCHREGTEST, name, cnxnMgr2, Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"), Hash256(), Hash256(), -1, -1.toBigInteger(), PlatformContext())
            if (restart) bchChain2.deleteAllPersistentData()
            cnxnMgr2.start()
            bchChain2.start()
            newWallet(name, bchChain2)
        }
         */

        run {
            val name = "BCH"
            val cnxnMgr2 = MultiNodeCnxnMgr(name, ChainSelector.BCHMAINNET, arrayOf("seed.bitcoinunlimited.net", "btccash-seeder.bitcoinunlimited.info"))
            cnxnMgr2.getElectrumServerCandidate = getElectrumServerCandidate
            val bchChain2 = Blockchain(
                ChainSelector.BCHMAINNET,
                name,
                cnxnMgr2,
                genesisBlockHash = Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
                checkpointPriorBlockId = Hash256("000000000000000001704aaa253740b7bb897b58378413c84f98066ca2f6bcc7"),
                checkpointId = Hash256("0000000000000000011fa4bf373415871bd227f03c3ef00ae541d36ce0bdf0dd"),
                checkpointHeight = 615608,
                checkpointWork = "11141541c85853109ca94e6".toBigInteger(16),
                context = context,
                dbPrefix = "BCHchain"
            )
            if (restart) bchChain2.deleteAllPersistentData()
            cnxnMgr2.start()
            bchChain2.start()
            blockchains[chainToURI[ChainSelector.BCHMAINNET].toString()] = bchChain2
            BCH = bchChain2
        }

        run {
            val name = "XEC"
            val cnxnMgr = MultiNodeCnxnMgr(name, ChainSelector.BCHMAINNET, arrayOf("192.168.1.165") )  // Returns BCH nodes: arrayOf("seed.bitcoinabc.org"))
            cnxnMgr.getElectrumServerCandidate = getXecElectrumServerCandidate
            val chain = Blockchain(
                ChainSelector.BCHMAINNET,
                name,
                cnxnMgr,
                genesisBlockHash = Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
                checkpointPriorBlockId = Hash256("000000000000000002e5b587bf4882a7cd7bdb8165aa6d7f4f7a3f91894ec8a2"),
                checkpointId = Hash256("00000000000000000ffff1834afe0ba850ba1f8360721a7f93790de89460abfc"),
                checkpointHeight = 703000,
                checkpointWork = "1553f75328a53c41d0296ee".toBigInteger(16),
                context = context,
                dbPrefix = "XECchain"
            )
            if (restart) chain.deleteAllPersistentData()
            cnxnMgr.start()
            chain.start()
            blockchains[name] = chain
        }

        // Start up the nxc blockchain
        run {
            val name = "XNEX"
            // local nextchain: val cnxnMgr2 = MultiNodeCnxnMgr(name, ChainSelector.NEXTCHAIN, arrayOf("192.168.1.100:7228"))
            val cnxnMgr = MultiNodeCnxnMgr(name, ChainSelector.NEXTCHAIN, arrayOf("seed.nextchain.cash", "node1.nextchain.cash", "node2.nextchain.cash"))
            cnxnMgr.desiredConnectionCount = 2  // XNEX chain doesn't have many nodes so reduce the desired connection count or there may be more desired nodes than exist in the chain
            cnxnMgr.getElectrumServerCandidate = getElectrumServerCandidate
            val chain = Blockchain(
                ChainSelector.NEXTCHAIN,
                name,
                cnxnMgr,
                genesisBlockHash = Hash256("78d2ee9c298e8a112e9e8e7ea1d878033e308494ccea107127413ede4f227cc5"),
                checkpointPriorBlockId = Hash256("78d2ee9c298e8a112e9e8e7ea1d878033e308494ccea107127413ede4f227cc5"),
                checkpointId = Hash256("0b64c9e1c30378582ba1e36a05a74a3c8fc016a1bbc9217950d8527e39403a5f"),
                checkpointHeight = 1,
                checkpointWork = 0x200101.toBigInteger(),
                context = context,
                dbPrefix = "XNEXchain"
            )
            if (restart) chain.deleteAllPersistentData()
            cnxnMgr.start()
            chain.start()
            // newWallet(name, chain)
            blockchains[chainToURI[ChainSelector.NEXTCHAIN].toString()] = chain
            XNEX = chain
        }

    }

    @JvmStatic
    fun main(args: Array<String>)
    {

        //val json = Json { encodeDefaults = true; ignoreUnknownKeys = true }
        //System.setProperty("java.util.logging.SimpleFormatter.format", "%1\$tY-%1\$tm-%1\$td %1\$tH:%1\$tM:%1\$tS %4$-6s %2\$s %5\$s%6\$s%n")
        //System.setProperty("java.util.logging.FileHandler.pattern", "dwally.log")

        LogIt.warning("Starting Wally Enterprise Wallet")

        InitializeJvmDesktopVariant()
        guiIni()

        //runBlocking { TestRequestMgr("bu", "bu", "localhost", 18332) }

        //? Current exchange rate between mBCH and your selected fiat currency
        var fiatPerMBCH: BigDecimal = 0.toBigDecimal(currencyMath).setScale(16)
        //? Currently selected fiat currency code
        var fiatCurrencyCode: String = "USD"

        //testSimpleEval(wallet)
        //testEngineFactory()
        if (!runTests)
        {
            initializeWallets(false)
        }
        else
        {
            GlobalScope.launch {
                // TODO missing reference: allTests("bu", "bu", "localhost", 18332)
            }
        }

        commandLoop()
    }

    fun newWallet(name: String, bc:Blockchain): Wallet
    {
        val w = runBlocking {
            val wdb: KvpDatabase = OpenKvpDB(PlatformContext(), name)!!  // One DB file per wallet for the desktop
            val w =
                try { Bip44Wallet(wdb, name) }
                catch (_: DataMissingException)
                {
                    Bip44Wallet(wdb, name, bc.chainSelector, NEW_WALLET)

                }
            w.addBlockchain(bc, bc.checkpointHeight, null)  // Since this is a new ram wallet (new private keys), there cannot be any old blocks with transactions
            wallet[name] = w
            w
        }
        return w
    }

    fun recoverWallet(name: String, recoveryKey: String, bc:Blockchain): Wallet
    {
        val w = runBlocking {
            val wdb: KvpDatabase = OpenKvpDB(PlatformContext(), name)!!  // One DB file per wallet for the desktop
            val w = Bip44Wallet(wdb, name, bc.chainSelector, recoveryKey)
            w.addBlockchain(bc, bc.checkpointHeight, null)  // Since this is a new ram wallet (new private keys), there cannot be any old blocks with transactions
            wallet[name] = w
            w
        }
        return w
    }


    fun openWallet(name: String, bc:Blockchain): Bip44Wallet
    {
        val w = runBlocking {
            val wdb = OpenKvpDB(PlatformContext(), name)!!  // One DB file per wallet for the desktop
            val w = try
            {
                Bip44Wallet(wdb, name)
            }
            catch (e: bitcoinunlimited.libbitcoincash.DataMissingException)
            {
                throw Error("Unknown wallet")
            }
            w.addBlockchain(bc, bc.checkpointHeight, null)  // Since this is a new ram wallet (new private keys), there cannot be any old blocks with transactions
            wallet[name] = w
            w
        }
        return w
    }

    fun help(operation: Any? = null): String
    {
        if (operation == null)
        {
            return """
!exit: quit
!reset: restart the script session
!help <expression>: Show help on whatever object type this expression resolves to
!: show command history
!<number>[append this text]: execute this history item.  You can add more script to execute by placing it after the number.
!x <file>: execute the specified file
!t <expression>: return the type of expression

newWallet(name, blockchain): Create a new wallet.
openWallet(name, blockchain): open a wallet.  ex. openWallet("foo", Blockchain.XNEX);
recoverWallet(name, recoveryKey, blockchain): recover a wallet from secret key
"""
        }
        else
        {
            return getApis(operation, style=EnumSet.of(DisplayStyle.Help, DisplayStyle.FieldPerLine)) ?: "No information available\n"
        }
    }

    fun displayObject(x:Any?): String
    {
        val ret = StringBuilder()

        if (x != null)
        {
            if (x is JsonElement)
            {
                val format = Json { prettyPrint = true }
                return format.encodeToString(x)
            }
            else if (x is String)
            {
                try  // See if it is json data and if so, pretty print
                {
                    val format = Json { prettyPrint = true }
                    val jsp = Json.parseToJsonElement(x)
                    return format.encodeToString(jsp)
                }
                catch (e: Exception)
                {
                }
            }
            ret.append(cliDump(x))
            if (!isPrimitive(x)) ret.append(getApis(x, style =EnumSet.of(DisplayStyle.OneLine)))
        }
        return ret.toString()
    }

    fun prompt(): String
    {
        var ret = ""
        for ((name,wal) in wallet)
        {
            ret += "(" + name + "@" + wal.syncedHeight + "/" + wal.blockchain.curHeight + " " + wal.balance.toString() + ":" + wal.balanceUnconfirmed.toString() + ")"
        }
        ret += " " + inputLines.size.toString() + "> "
        return ret
    }

    fun anotherReadLine(): String
    {
        val scanner = Scanner(System.`in`)
        return scanner.nextLine()
    }

    @OptIn(ExperimentalPathApi::class)
    fun commandLoop()
    {
        var injectCommand: String? = null
        while(true)
        {
            var historyOverride: String? = null
            if (engine == null) engine = scriptSession()
            print(prompt())
            var str:String? = if (injectCommand == null) anotherReadLine() else injectCommand
            injectCommand = null

            if (str != null)
            {
                if (str == "?" || str == "help")
                {
                    print(help(null))
                    str = null  // consumed it
                }
                else if (str == "!exit") return
                else if (str == "!reset") engine = null
                else if (str.startsWith("!help") || (str.length > 0 && str[0] == '?'))
                {
                    val expression = str.substringAfter(' ').trim({it.isWhitespace()})
                    val result = eval(expression)
                    print(help(result) + "\n")
                    str = null
                }
                else if (str.length > 0 && str[0] == '!')
                {
                    var cmd = str.drop(1)
                    var num:Int? = null
                    str = null  // This isn't a normal kts line to be executed so clear it out

                    // pick out the digits in the string and convert to a number, leaving the rest to be appended to the retrieved history
                    if (cmd.length > 0)
                    {
                        val numEnd = cmd.indexOfFirst { c -> !(c >= '0' && c <= '9') }
                        num = if (numEnd != -1)
                        {
                            val numpart = cmd.substring(0, numEnd)
                            cmd = cmd.substring(numEnd)
                            numpart.toIntOrNull()
                        }
                        else
                        {
                            val n = cmd.toIntOrNull()
                            cmd = ""
                            n
                        }
                    }

                    if (num != null)
                    {
                        if (num < inputLines.size)
                        {
                            print("--> " + inputLines[num] + cmd + "\n")
                            injectCommand = inputLines[num] + cmd
                        }
                        else
                        {
                            print("history out of range")
                        }
                    }
                    else if (cmd.length == 0)  // display the history
                    {
                        inputLines.forEachIndexed { index, line -> print(index.toString() + ": " + line + "\n") }
                        str = null  // consumed
                    }
                    else if (cmd[0] == 'p') // new panel
                    {
                        guiBlockchainPanel(BCH)
                        //guiNewPanel()
                        str = null // consumed it
                    }
                    else if (cmd[0] == 't')
                    {
                        str = null
                        var expression = cmd.drop(1).trim({it.isWhitespace()})
                        expression += "::class.simpleName"
                        print("--> executing ${expression}\n")
                        eval(expression)
                    }
                    else if (cmd[0] == 'x')
                    {
                        str = null
                        var fileName = cmd.drop(1).trim({it.isWhitespace()})
                        if (fileName.length == 0) fileName = lastFile

                        if (fileName[0] == '/')  // Don't search the path
                        {
                            var f = File(fileName)
                            if (!f.isFile())
                            {
                                fileName = fileName + ".kts"
                                f = File(fileName)
                            }
                            if (!f.isFile()) print("cannot find ${fileName.dropLast(3)} or ${fileName}\n")
                            else
                            {
                                str = f.readText()
                                print("--> executing ${fileName} last modified on ${Date(f.lastModified())}\n")
                            }
                        }
                        else
                        {
                            for (p in scriptSearchPath)
                            {
                                var fpath = Path(p)
                                fpath = fpath.resolve(fileName)

                                var f = fpath.toFile()
                                if (!f.isFile())
                                {
                                    fpath = Path(p).resolve(fileName + ".kts")
                                    f = fpath.toFile()
                                }
                                if (f.isFile())
                                {
                                    str = f.readText()
                                    fileName = fpath.toString()
                                    print("--> executing ${fileName} last modified on ${Date(f.lastModified())}\n")
                                    break
                                }
                            }
                        }
                        if (str == null) print("cannot find ${fileName.dropLast(3)} or ${fileName} in path ${scriptSearchPath}\n")
                        else
                        {
                            historyOverride = "!x ${fileName}"
                        }

                    }
                }

                if (str != null)
                {
                    eval(str, historyOverride)
                }
            }
            else  // file was closed
            {
                return
            }
        }
    }

    fun scriptSession(): ScriptEngine  // KotlinJsr223JvmLocalScriptEngine
    {
        val sem = ScriptEngineManager()
        val factories = sem.getEngineFactories()
        /*
        for (i in factories)
        {
            print(i.engineName)
            print(i.extensions)
        }
         */

        //val ret = sem.getEngineByExtension("kts") as KotlinJsr223JvmLocalScriptEngine
        val ret = sem.getEngineByExtension("kts")
        //val ret = KotlinJsr223JvmDaemonLocalEvalScriptEngineFactory().scriptEngine
        //val ret = KotlinJsr223JvmLocalScriptEngineFactory().scriptEngine

        ret.eval("import bitcoinunlimited.libbitcoincash.*")
        ret.eval("import kotlin.coroutines.CoroutineContext")
        ret.eval("import kotlinx.serialization.json.*")  // For JsonElement and related classes
        ret.eval("import java.util.EnumSet")
        ret.put("wallets", wallet)
        ret.eval("""val wallets = bindings["wallets"] as MutableMap<String,Wallet>""")
        ret.put("""newWallet""", { name: String, blockchain: Blockchain -> newWallet(name, blockchain)})
        ret.put("""recoverWallet""", { name: String, recoveryKey: String, blockchain: Blockchain -> recoverWallet(name, recoveryKey, blockchain)})
        ret.put("""openWallet""", { name: String, blockchain: Blockchain -> openWallet(name, blockchain)})
        ret.put("""easyJson""", { je:JsonElement -> EJ(je) })
        ret.put("""help""", { operation:Any? -> help(operation) })
        ret.put("result", null)
        ret.eval("""val result: Any? get() = bindings["result"]""")
        ret.put("coCtxt", coCtxt)
        ret.eval("""fun newWallet(name:String, chain:Blockchain): Bip44Wallet { return (bindings["newWallet"] as (String, Blockchain) -> Bip44Wallet)(name,chain) }""")
        ret.eval("""fun openWallet(name:String, chain:Blockchain): Bip44Wallet { return (bindings["openWallet"] as (String, Blockchain) -> Bip44Wallet)(name,chain) }""")
        ret.eval("""fun recoverWallet(name:String, recoveryKey: String, chain:Blockchain): Bip44Wallet { return (bindings["recoverWallet"] as (String, String, Blockchain) -> Bip44Wallet)(name, recoveryKey, chain) }""")
        ret.eval("""fun help(about:Any?=null):String { return (bindings["help"] as (Any?) -> String)(about) }""")
        ret.eval("""fun easy(je:JsonElement): EJ { return (bindings["easyJson"] as (JsonElement) -> EJ)(je) }""")

        ret.eval("""fun <T> runBlocking(x:suspend kotlinx.coroutines.CoroutineScope.() -> T):T { return kotlinx.coroutines.runBlocking(bindings["coCtxt"]!! as CoroutineContext,x) }""")
        //fun <T> split(x:()->T): Thread { return kotlin.concurrent.thread(true, true, null, "split") { x() } }
        ret.eval("""fun <T> split(x:()->T): Thread { return kotlin.concurrent.thread(true, true, null, "split") { x() } }""")
        ret.put("blockchains", blockchains)
        ret.eval("""val blockchains = bindings["blockchains"] as MutableMap<String,Blockchain>""")
        ret.put("p", { x:Any? -> displayObject(x) })
        ret.eval("""val p = bindings["p"] as (kotlin.Any?) -> kotlin.String""")

        // Specific useful setup
        ret.eval("val NEXTCHAIN=ChainSelector.NEXTCHAIN")
        ret.eval("val BITCOINCASH=ChainSelector.BCHMAINNET")
        ret.eval("""val XNEX = blockchains["${chainToURI[ChainSelector.NEXTCHAIN]}"]!!""")
        ret.eval("""val BCH = blockchains["${chainToURI[ChainSelector.BCHMAINNET]}"]!!""")

        return ret
    }

    fun eval(cmd:String, inputLineOverride: String? = null):Any?
    {
        inputLines.add(if (inputLineOverride == null) cmd else inputLineOverride)
        val eng = engine
        if (eng != null)
        {
                try
                {
                    //print("Cmd: $cmd")
                    //print(" Eng: $eng\n")
                    val ret: Any? = eng.eval(cmd)
                    if (ret != null)
                    {
                        print(ret.toString() + "\n")
                    }
                    eng.put("result", ret)
                    return ret
                } catch (e: ScriptException)
                {
                    //print("Script Exception\n")
                    var realException = e.cause ?: e

                    val os = ByteArrayOutputStream()
                    realException.printStackTrace(PrintStream(os))
                    print("Exception ${e.message} at ${e.fileName}:${e.lineNumber}.${e.columnNumber}\n")
                    print(os.toString())
                    eng.put("lastException", realException)
                    eng.eval("""val lastException = bindings["lastException"] as Exception""")
                    return realException
                } catch (e: Exception)
                {
                    //print("Exception\n")
                    val os = ByteArrayOutputStream()
                    e.printStackTrace(PrintStream(os))
                    LogIt.info(os.toString("UTF8"))
                    print(os.toString("UTF8"))
                    eng.put("lastException", e)
                    eng.eval("""val lastException = bindings["lastException"] as Exception""")
                    return e
                }
        }
        return null
    }

    fun testSimpleEval(wallet: Wallet)
    {
        val engine = ScriptEngineManager().getEngineByExtension("kts") as KotlinJsr223JvmLocalScriptEngine
        engine.put("wallets", wallet)
        val res3 = engine.eval("import bitcoinunlimited.libbitcoincash.*")
        val res4 = engine.eval("""val wallets = (bindings["wallets"] as Wallet)""")
        engine.eval("val d = wallet.newDestination()")
        engine.eval("print(d.GetAddress().toString())")
    }
    fun testEngineFactory()
    {
        val a = ScriptEngineManager()
        val engine = a.getEngineByExtension("kts")
        val factory = engine.factory
        //Assert.assertNotNull(factory)
        factory?.apply {
            print(languageName)
            print(languageVersion)
            //Assert.assertEquals("kotlin", engineName)
            //Assert.assertEquals(KotlinCompilerVersion.VERSION, engineVersion)
            //Assert.assertEquals(listOf("kts"), extensions)
            //Assert.assertEquals(listOf("text/x-kotlin"), mimeTypes)
            //Assert.assertEquals(listOf("kotlin"), names)
            //Assert.assertEquals("obj.method(arg1, arg2, arg3)", getMethodCallSyntax("obj", "method", "arg1", "arg2", "arg3"))
            //Assert.assertEquals("print(\"Hello, world!\")", getOutputStatement("Hello, world!"))
            //Assert.assertEquals(KotlinCompilerVersion.VERSION, getParameter(ScriptEngine.LANGUAGE_VERSION))
            val sep = System.getProperty("line.separator")
            val prog = arrayOf("val x: Int = 3", "var y = x + 2")
            //Assert.assertEquals(prog.joinToString(sep) + sep, getProgram(*prog))
        }
        //engine.eval("val p = 2")
        //print(engine.eval("p+2"))
    }
}

class InitializeJvmDesktopVariant:Initialize()
{
    companion object
    {
        // Used to load the 'native-lib' library on application startup.
        init
        {
            val javaLibPath = System.getProperty("java.library.path")
            System.out.println("Searching for libbitcoincash.so in:" + javaLibPath)
            try
            {
                System.loadLibrary("bitcoincash")
            }
            catch (e: UnsatisfiedLinkError)
            {
                System.out.println("Error ${e.toString()}")
                System.out.println("Likely libbitcoincash.so was NOT found.")
                throw e
            }
            System.out.println("Libbitcoincash.so loaded.")

            try
            {
                LibBitcoinCash(DEFAULT_CHAIN)
            }
            catch (e: UnsatisfiedLinkError)
            {
                System.out.println("Error ${e.toString()}")
                System.out.println("Likely libbitcoincash.so was NOT configured with the --enable-javacashlib flag enabled.")
                throw e
            }

            val searchPath = System.getenv("WEW_SCRIPT_PATH")
            if (searchPath != null)
            {
                scriptSearchPath = searchPath.split(":")
            }


        }
    }
}

