// Copyright (c) 2021 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash


/** A wallet that is only stored in RAM, for use in tests.  New addresses/secrets are created using the Android/Java secure random number generator (SecRandom).
 *  Pass the coin's ticker that it represents (for logging), and the point at which you want to start syncing from
 *  (that is, this wallet will not look at blocks prior to 'syncedTo for transactions
 */
class RamKvpDao: KvpDao
{
    val db = mutableMapOf<ByteArray, ByteArray>()
    //@Query("SELECT * FROM KvpData WHERE id = :key")
    override fun get(key: ByteArray): KvpData?
    {
        val v = db.get(key)
        if (v != null)
        {
            return KvpData(key, v)
        }
        return null
    }

    //@Insert
    override fun insert(bh: KvpData)
    {
        if (db.get(bh.id) != null) throw RuntimeException("element exists")  // In other DBs insert MUST not overwrite.  Copy this functionality
        db.set(bh.id, bh.value)
    }

    //@Update
    override fun update(bh: KvpData)
    {
        db[bh.id] = bh.value
    }

    //@Delete
    override fun delete(bh: KvpData)
    {
        db.remove(bh.id)
    }

    //@Query("DELETE FROM KvpData")
    override fun deleteAll() = db.clear()
}
class RamKvpDatabase:KvpDatabase()
{
    val kvpdao = RamKvpDao()
    override fun dao(): KvpDao
    {
        return kvpdao
    }
}


class RamWallet(name: String, chainSelector: ChainSelector) : CommonWallet(name, chainSelector)
{
    /** This secret data is used as entropy when generating seeded destinations (seeded destinations are a destination that, given a seed, always produces the
     * same destination. */
    val secretGen: ByteArray = ByteArray(32)
    val backingStore = RamKvpDatabase()

    init
    {
        SecRandom(secretGen)
        walletDb = backingStore
        LONG_DELAY_INTERVAL = 500  // RamWallet is only used in a test environment so use quick delays
    }

    override fun generateDestination(): PayDestination
    {
        return Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(SecRandom(ByteArray(32))))
    }

    override fun destinationFor(seed: String): PayDestination
    {
        val generator: ByteArray = seed.toByteArray() + secretGen
        val secret = UnsecuredSecret(Hash.hash256(generator))
        return Pay2PubKeyHashDestination(chainSelector, secret)
    }


    /** Save wallet transaction state to the database -- NO-OP for the RAM wallet */
    override fun saveWalletTo(db: KvpDatabase?)
    {
        return
    }

    /** Load wallet transaction state from the database */
    override fun loadWalletTx(db: KvpDatabase): Boolean
    {
        return true
    }
}
