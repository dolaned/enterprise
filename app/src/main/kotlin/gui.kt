// Copyright (c) 2021 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.enterprise;

import androidx.compose.desktop.AppManager
import androidx.compose.desktop.Window
import androidx.compose.desktop.WindowEvents
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.Alignment
import androidx.compose.ui.unit.*
import org.jetbrains.skija.Image
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.File
import javax.imageio.ImageIO

import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
//import com.google.zxing.client.j2se.MatrixToImageConfig
//import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel

import bitcoinunlimited.libbitcoincash.Blockchain
import bitcoinunlimited.libbitcoincash.*
import com.google.zxing.MultiFormatWriter
import kotlinx.serialization.json.Json
import org.jetbrains.skija.Bitmap

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color

private val LogIt = GetLog("BU.enterpriseWallet.gui")


fun guiIni()
{
    AppManager.setEvents(
        // onAppStart = { println("onAppStart") }, // Invoked before the first window is created
        onAppExit = {  } // Override the default handler which shuts down the entire app
    )
}

fun getWindowIcon(): BufferedImage
{
    var image: BufferedImage? = null
    try
    {
        image = ImageIO.read(File("media/bitcoin_cash_token.png"))
    }
    catch (e: Exception)
    {
        // image file does not exist
    }

    if (image == null)
    {
        image = BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB)
    }

    return image
}

// @Composable
fun guiBlockchainPanel(bc: Blockchain)
{
    val w = Window(
        icon = getWindowIcon(),
        title = "Blockchain ${bc.name} (${bc.chainSelector})",
        // size = IntSize(300, 300)
        events = WindowEvents(
            onClose = {  }
        )) {
            val count = remember { mutableStateOf(0) }
            MaterialTheme {
                Column(Modifier.fillMaxSize(), Arrangement.spacedBy(5.dp)) {
                    Button(modifier = Modifier.align(Alignment.CenterHorizontally),
                        onClick = {
                            count.value++
                        }) {
                        Text(if (count.value == 0) "Hello World" else "Clicked ${count.value}!")
                    }
                    Button(modifier = Modifier.align(Alignment.CenterHorizontally),
                        onClick = {
                            count.value = 0
                        }) {
                        Text("Reset")
                    }
                    Canvas(modifier = Modifier.fillMaxSize()) {
                        drawRect(Color.Blue, topLeft = Offset(0f, 0f), size = Size(this.size.width, 55f))
                        drawCircle(Color.Red, center = Offset(50f, 200f), radius = 40f)
                        drawLine(
                            Color.Green, Offset(20f, 0f),
                            Offset(200f, 200f), strokeWidth = 5f
                        )

                        drawArc(
                            Color.Black,
                            0f,
                            60f,
                            useCenter = true,
                            size = Size(300f, 300f),
                            topLeft = Offset(60f, 60f)
                        )
                    }
                }
            }
        }

}

// @Composable
fun guiNewPanel()
{
    val w = Window(
        events = WindowEvents(
            onClose = {  })
    ) {
        var text by remember { mutableStateOf("Hello, World!") }


        MaterialTheme {
            Button(onClick = { text = "Hello, Desktop!" }) {
                Text(text)
            }
        }
    }
}

/*
private fun generateQRCode(text: String): Image? {
    val width = 500
    val height = 500

    val codeWriter = MultiFormatWriter()
    try {
        val bitMatrix = codeWriter.encode(text, BarcodeFormat.QR_CODE, width, height)
        /*
        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
            }
        }
         */
        val image = Image.makeFromEncoded(bitMatrix.???)
        return image
    } catch (e: WriterException) {
        //Log.d(TAG, "generateQRCode: ${e.message}")
    }
    return null
}
*/