import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    gradlePluginPortal()
    mavenCentral()
    mavenLocal()
    google()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
}

plugins {
    kotlin("jvm") version "1.5.21"
    //id("org.jetbrains.compose") version "1.0.0-alpha2"
    id("org.jetbrains.compose") version "0.5.0-build270"
    application
    distribution
}

dependencies {
    //implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    //implementation "info.bitcoinunlimited:libbitcoincash:0.2.1"
    //implementation(files(File("jars/libbitcoincashjvm.jar"))) // locate this directory at: <repo_home>/jars/
    //classpath("org.jetbrains.compose:compose-gradle-plugin:1.0.0-alpha1")
    implementation(project(":app"))
    implementation(project(":libbitcoincashjvm", "default"))
}

application {
   mainClass.set("bitcoinunlimited.enterprise.DesktopWallet")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}